﻿[著作権ライセンス / License]
CC-BY 2.1 JP
http://creativecommons.org/licenses/by/2.1/jp/

[著作権者 / Copyright]
Copyright (C) 2013 sacorama.

[原著作権者 / Original Copyright]
Copyright (C) 2013 Butameron. (配置図、ラフイメージ）

[連絡先 / Contact]
