Copyright (C) OPAP-JP contributors.

This work consists of voluntary contributions made by many individuals.

* For exact contributors list, see all AUTHORS.txt located in this directory
  and all sub-directories.

* For contribution history, see the revision history available at 
  https://git.opap.jp/projects/KOSYS/repos/kosys-ep02


The following license applies to all parts of this work except as documented 
below:

===========================================================

* Creative Commons Attribution 2.1 JP
  http://creativecommons.org/licenses/by/2.1/jp/ 

* Creative Commons Attribution 4.0 International
  http://creativecommons.org/licenses/by/4.0/

* OPAP Upgrade License 1.0 Japan
  http://opap.jp/wiki/Licenses/OPAP-UP/1.0/jp 

===========================================================

If there is different LICENSE.txt, its terms apply to all files located in the
same dierctory and sub-directories. Their terms may differ from this terms 
above.



********************************************************************************
* 日本語訳（Translation into Japanese of the above)
********************************************************************************

Copyright (C) OPAP-JP 貢献者.

この作品は多くの人々の自発的な貢献により構成されています。

* 正確な貢献者リストについては、このフォルダおよびサブフォルダにあるすべての
  AUTHORS.txtを参照してください。

* 正確な貢献の履歴は、次の更新履歴を参照して下さい。
  https://git.opap.jp/projects/KOSYS/repos/kosys-ep02


後述する例外を除き、この作品のすべての部分に次の利用規約が適用されます。

===========================================================

* クリエイティブ・コモンズ 表示 2.1 日本
  http://creativecommons.org/licenses/by/2.1/jp/ 

* クリエイティブ・コモンズ 表示 4.0 国際
  http://creativecommons.org/licenses/by/4.0/deed.ja

* OPAP Upgrade License 1.0 Japan
  http://opap.jp/wiki/Licenses/OPAP-UP/1.0/jp 

===========================================================

もし異なるLICENSE.txtが存在する場合、その規約は同じフォルダまたはサブフォルダに
存在するすべてのファイルに適用されます。それらの規約は上記の規約とは異なる場合が
あります。


